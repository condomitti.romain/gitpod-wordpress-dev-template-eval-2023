<?php
add_action( 'wp_enqueue_scripts', 'iut_enqueue_scripts' );
function iut_enqueue_scripts() {
	$parenthandle = 'twentynineteen-style'; // This is 'twentynineteen-style' for the Twenty Fifteen theme.
	$theme        = wp_get_theme();

    //load-parent   
	wp_enqueue_style( $parenthandle,
		get_template_directory_uri() . '/style.css', // https://iut.org-content/iut/twentynineteen/style.css
		array(),  // If the parent theme code has a dependency, copy it to here.
		$theme->parent()->get( 'Version' )
	);

    // load child css in theme
	wp_enqueue_style( 'twentynineteen-style',
		get_stylesheet_uri(),
		array( $parenthandle ),
		$theme->get( 'Version' ) // This only works if you have Version defined in the style header.
	);
}

function wporg_custom_post_type() {
	register_post_type('wporg_product',
		array(
			'labels'      => array(
				'name'          => __( 'Recettes', 'textdomain' ),
				'singular_name' => __( 'Recette', 'textdomain' ),
			),
			'public'      => true,
			'has_archive' => true,
			'show_in_rest' => true,
			'rewrite'     => array( 'slug' => 'recette' ), // my custom slug
			'has_archive' => 'reccettes'
		) 
	);
}
add_action('init', 'wporg_custom_post_type');
abstract class WPOrg_Meta_Box {


	/**
	 * Set up and add the meta box.
	 */
	public static function add() {
		$screens = [ 'post', 'wporg_cpt' ];
		foreach ( $screens as $screen ) {
			add_meta_box(
				'wporg_box_id',          // Unique ID
				'info complémentaire', // Box title
				'iut_mbox_recette_content',   // Content callback, must be of type callable
				'recette'                // Post type
			);
		}
	}


	/**
	 * Save the meta box selections.
	 *
	 * @param int $post_id  The post ID.
	 */
	public static function save( int $post_id ) {
		if( isset ($_POST['iut-ingredient']) && !empty( $_POST['iut-ingredient']) ){
			update_post_meta($post_ID, 'iut-ingredient', sanitize_text_field($_POST['iut-ingredient']));
		}
	}


	/**
	 * Display the meta box HTML to the user.
	 *
	 * @param WP_Post $post   Post object.
	 */
	public static function html( $post ) {
		$recette = get_post_meta( $post->ID, 'iut-ingredient', true );
		?>
		<label for="wporg_field">Description for this field</label>
		<textarea id = "iut-ingredient" name = "iut-ingredient" value = "'.$iut_ingredient.'"></textarea>
		<?php
	}
}

add_action( 'save_post', [ 'WPOrg_Meta_Box', 'save' ] );
add_action( 'add_meta_boxes', [ 'WPOrg_Meta_Box', 'add' ] );
